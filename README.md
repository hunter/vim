# The ultimate configuration of Vim

The awesome configurations and color schemes that make Vim a lot better. To
install it simply do following from your terminal.

```bash
mkdir -p /etc/vim/autoload
mkdir -p /etc/vim/bundle
mkdir -p /etc/vim/colors
mkdir -p /usr/share/vim/vimfiles/autoload
mkdir -p /usr/share/vim/vimfiles/bundle
mkdir -p /usr/share/vim/vimfiles/colors

curl -Lso /etc/vim/vimrc https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc
curl -Lso /etc/vim/vimrc.local https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc.local
curl -Lso /etc/vim/colors/corporation.vim https://gitlab.com/hunter/vim/raw/master/etc/vim/colors/corporation.vim

cp -rf /etc/vim/vimrc /usr/share/vim
cp -rf /etc/vim/vimrc.local /usr/share/vim
cp -rf /etc/vim/colors/*.vim /usr/share/vim/vimfiles/colors

cat /etc/vim/vimrc > ~/.vimrc
```

OpenWrt

```bash
mkdir -p /usr/share/vim/vimfiles/colors
curl -Lso /usr/share/vim/vimrc https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc
curl -Lso /usr/share/vim/vimrc.local https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc.local
curl -Lso /usr/share/vim/vimfiles/colors/corporation.vim https://gitlab.com/hunter/vim/raw/master/etc/vim/colors/corporation.vim

cat /usr/share/vim/vimrc > ~/.vimrc
```

Scoop - A command-line installer for Windows

```powershell
[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"

mkdir "$env:SCOOP\persist\vim\vimfiles\colors"

Invoke-WebRequest -OutFile "$env:SCOOP\persist\vim\vimrc" -Uri "https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc.local"
Invoke-WebRequest -OutFile "$env:SCOOP\persist\vim\vimrc.local" -Uri "https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc.local"
Invoke-WebRequest -OutFile "$env:SCOOP\persist\vim\vimfiles\colors\corporation.vim" -Uri "https://gitlab.com/hunter/vim/raw/master/etc/vim/colors/corporation.vim"
```

Neovim

```bash
# Unix
mkdir -p "${HOME}/.config/nvim/colors"
curl -Lso "${HOME}/.config/nvim/init.vim" https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc.local
curl -Lso "${HOME}/.config/nvim/colors/corporation.vim" https://gitlab.com/hunter/vim/raw/master/etc/vim/colors/corporation.vim
```

```powershell
# Windows
mkdir %UserProfile%\AppData\Local\nvim\colors
Invoke-WebRequest -OutFile %UserProfile%\AppData\Local\nvim\init.vim https://gitlab.com/hunter/vim/raw/master/etc/vim/vimrc.local
Invoke-WebRequest -OutFile %UserProfile%\AppData\Local\nvim\colors\corporation.vim https://gitlab.com/hunter/vim/raw/master/etc/vim/colors/corporation.vim
```
